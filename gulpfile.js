const gulp = require("gulp");
const zip = require("gulp-zip");

// Todo add name replacement for themename. _ is not allowed as starting character in block name
// Also for Themename ( i.e. capitalized version of the theme name presented to the user. Right now only in plugin.php

function bundle() {
    return gulp
        .src([
            "**/*",
            "!node_modules/**",
            "!src/**",
            "!bundled/**",
            "!gulpfile.js",
            "!package.json",
            "!package-lock.json",
            "!webpack.config.js",
            "!.gitignore",
            "!.eslintrc.js"
        ])
        .pipe(zip("themename-blocks.zip"))
        .pipe(gulp.dest("bundled"));
}

exports.bundle = bundle;
