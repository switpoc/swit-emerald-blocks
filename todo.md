## Check 
npx prettier --check "src/**/*.js"

## Change 
npx prettier --write "src/**/*.js"



## Second block
deprecated
replace down whith upgraded, install them using npm i -D and add as externals to webpack and as deps to plugin and import them.


wp.editor.getColorClassName is deprecated. Please use wp.blockEditor.getColorClassName instead.

wp.editor.RichText.Content is deprecated. Please use wp.blockEditor.RichText.Content instead.

wp.editor.InspectorControls is deprecated. Please use wp.blockEditor.InspectorControls instead.

wp.editor.BlockControls is deprecated. Please use wp.blockEditor.BlockControls instead.

wp.editor.RichText is deprecated. Please use
wp.blockEditor.RichText instead.

wp.blockEditor.RichText formattingControls prop is deprecated. Please use allowedFormats instead.

wp.editor.AlignmentToolbar is deprecated. Please use wp.blockEditor.AlignmentToolbar instead.

wp.editor.PanelColorSettings is deprecated. Please use wp.blockEditor.PanelColorSettings instead.

wp.editor.ContrastChecker is deprecated. Please use wp.blockEditor.ContrastChecker instead.