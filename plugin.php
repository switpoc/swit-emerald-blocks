<?php
/*
Plugin Name:  themename-blocks
Plugin URI:   https://skillworksit.se
Description:  Adding Blocks for themename
Version:      1.0.0
Author:       George Bredberg
Author URI:   https://www.skillworksit.se
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  themename-blocks
Domain Path:  /languages
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// demo old metabox behaviour. Install classic-editor-plugin and test
include_once('src/metabox.php');

function themename_blocks_categories( $categories, $post ) {
	//$post if we need to filter on different post types. Not used right now.
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'themename-category',
				'title' => __( 'Themename Category', 'themename-blocks' ),
				'icon'  => 'feedback'
			)
		)
	);
}

add_filter( 'block_categories', 'themename_blocks_categories', 10, 2 );

function themename_blocks_register_block_type( $block, $options = array() ) {
	// blockname has to start with a letter, not _
	register_block_type(
		'themename-blocks/' . $block,
		array_merge(
			array(
				'editor_script' => 'themename-blocks-editor-script',
				'editor_style'  => 'themename-blocks-editor-style',
				'script'        => 'themename-blocks-script',
				'style'         => 'themename-blocks-style'
			),
			$options
		)
	);
}

function themename_blocks_enqueue_assets() {
	wp_enqueue_script(
		'themename-blocks-editor-js',
		plugins_url('dist/editor_script.js', __FILE__),
		array('wp-data', 'wp-plugins', 'wp-edit-post', 'wp-i18n')
	);
}

add_action('enqueue_block_editor_assets', 'themename_blocks_enqueue_assets');

function themename_blocks_register() {
	wp_register_script(
		'themename-blocks-editor-script',
		plugins_url( 'dist/editor.js', __FILE__ ),
		array(
			'wp-blocks',
			'wp-i18n',
			'wp-element',
			'wp-editor',
			'wp-components',
			'lodash',
			'wp-blob',
			'wp-data',
			'wp-html-entities',
			'wp-compose'
		)
	);

	wp_register_script(
		'themename-blocks-script',
		plugins_url( 'dist/script.js', __FILE__ ),
		array( 'jquery' )
	);

	wp_register_style( 'themename-blocks-editor-style',
		plugins_url( 'dist/editor.css', __FILE__ ),
		array( 'wp-edit-blocks' ) );

	wp_register_style(
		'themename-blocks-style',
		plugins_url( 'dist/style.css', __FILE__ )
	);

	themename_blocks_register_block_type( 'firstblock' );
	themename_blocks_register_block_type( 'secondblock' );
	themename_blocks_register_block_type( 'team-member' );
	themename_blocks_register_block_type( 'team-members' );
	themename_blocks_register_block_type( 'latest-posts', array(
		'render_callback' => 'themename_blocks_render_latest_posts_block',
		'attributes'      => array(
			'numberOfPosts'  => array(
				'type'    => 'number',
				'default' => 5
			),
			'postCategories' => array(
				'type' => 'string'
			)
		)
	) );
	themename_blocks_register_block_type( 'redux' );
	themename_blocks_register_block_type( 'todo-list' );
	themename_blocks_register_block_type( 'todo-list-count' );
	themename_blocks_register_block_type( 'meta' );
}

add_action( 'init', 'themename_blocks_register' );

function themename_blocks_render_latest_posts_block( $attributes ) {
	$args  = array(
		'posts_per_page' => $attributes['numberOfPosts']
	);
	if($attributes['postCategories']) {
		$args['cat'] = $attributes['postCategories'];
	}

	$query = new WP_Query( $args );
	$posts = '';

	if ( $query->have_posts() ) {
		$posts .= '<ul class="wp-block-themename-blocks-latest-posts">';
		while ( $query->have_posts() ) {
			$query->the_post();
			$posts .= '<li><a href="' . esc_url( get_the_permalink() ) . '">' . get_the_title() . '</a></li>';
		}

		$posts .= '</ul>';
		wp_reset_postdata();

		return $posts;
	} else {
		return '<div>' . __( "No Posts Found", "themename-blocks" ) . '</div>';
	}
}

// register an existing block so it automatically shows up in the editor
// if you are registering a posttype you can do this in the registering function, but since we
// are modifying an existing posttype we need to use the init action to access the post object.
function mytheme_blocks_register_post_template() {
	$post_type_object = get_post_type_object( 'post' );
	$post_type_object->template = array(
		array('themename-blocks/meta'),
		// adds a predefined paragraph to our post (that can be edited)
		array('core/paragraph', array(
			'content' => 'Demo (predefined) content'
		)),
		// add a members-block (example of adding nested blocks
		array(
			'themename-blocks/team-members',
			array(
				'columns' => 2
			),
			array(
				array('themename-blocks/team-member', array('title' => 'Team member')),
				array('themename-blocks/team-member'),
			)
		)
	);
	//$post_type_object->template_lock = 'all'; //cant add or modify (drag) blocks
	//$post_type_object->template_lock = 'insert'; //cant add blocks
}
add_action('init', 'mytheme_blocks_register_post_template');