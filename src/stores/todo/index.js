import { registerStore } from "@wordpress/data";

const DEFAULT_STATE = [
    /*    {
        userId: 1,
        id: 1,
        title: "Important task one",
        completed: false
    },
    {
        userId: 1,
        id: 2,
        title: "Important task two",
        completed: false
    }*/
];

const actions = {
    populateToDos(todos) {
        return {
            type: "POPULATE_TODOS",
            todos
        };
    },
    addToDo(item) {
        return {
            type: "ADD_TODO",
            item: item
        };
    },
    fetchToDos() {
        return {
            type: "FETCH_TODOS"
        };
    },
    // Need to use generator function when doing an api call
    *toggleToDo(todo, index) {
        // Disable checkbox while waiting for down response
        yield {
            type: "UPDATE_TODO",
            index,
            todo: { ...todo, loading: true }
        };
        const response = yield {
            type: "TOGGLE_TODO",
            todo
        };
        return {
            type: "UPDATE_TODO",
            index,
            todo: response
        };
    }
};

const reducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case "ADD_TODO":
            return [...state, action.item];
        case "POPULATE_TODOS":
            return [...action.todos];
        case "UPDATE_TODO": {
            let state_copy = [...state];
            state_copy[action.index] = action.todo;
            return state_copy;
        }
        default:
            return state;
    }
};

// Don't do side effects here, add the selector function name to the resolvers instead.
// It will be run when the selector get triggered
const selectors = {
    getToDos(state) {
        return state;
    },
    getToDosNumber(state) {
        return state.length;
    },
    getUnDoneToDosNumber(state) {
        return state.filter(todo => !todo.completed).length;
    },
    getDoneToDosNumber(state) {
        return state.filter(todo => todo.completed).length;
    }
};

registerStore("themename-blocks/todo", {
    reducer,
    selectors,
    actions,
    controls: {
        FETCH_TODOS() {
            return fetch("https://jsonplaceholder.typicode.com/todos?_limit=10").then(response =>
                response.json()
            );
        },
        TOGGLE_TODO({ todo }) {
            return fetch(`https://jsonplaceholder.typicode.com/todos/${todo.id}`, {
                method: "PATCH",
                body: JSON.stringify({
                    completed: !todo.completed
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }).then(response => response.json());
        }
    },
    resolvers: {
        *getToDos() {
            const toDos = yield actions.fetchToDos();
            return actions.populateToDos(toDos);
        }
    }
});

// Try in browser console with
// wp.data.select('themename-blocks/todo').getToDos();
// wp.data.dispatch('themename-blocks/todo').addToDo('test');
