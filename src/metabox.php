<?php

// Register metafield for it to show up in redux
function themename_blocks_register_meta() {
	register_meta( 'post', '_themename_blocks_post_subtitle', array(
		'show_in_rest' => true,
		'type'         => 'string',
		'single'       => true,
		'sanitize_callback' => 'sanitize_text_field',
		// To allow users that can edit post to update this field. Normally a field that starts with _ is a private field
        // and can't be updated via the rest-api
        'auth_callback' => function() {
		    return current_user_can('edit_posts');
		}
	) );
}

add_action( 'init', 'themename_blocks_register_meta' );
// test above with
// wp.data.select('core/editor').getEditedPostAttribute('meta')
// wp.data.dispatch('core/editor').editPost({meta: {_themename_blocks_post_subtitle: 'Subtitle via metafield'}})

// Demo old metabox behaviour. Ads a field for subtitle
function themename_blocks_add_meta_box() {
	add_meta_box(
		'themename_blocks_post_options_metabox',
		'Post Options',
		'themename_blocks_post_options_metabox_html',
		'post',
		'normal',
		'default',
		// if down setting is true, the metabox will NOT show up in Guthenberg. I.E. you handle the meta fields there in another way.
		// if false, it will show up, but will be handled by an extra PHP call, not by Redux.
		array( '__back_compat_meta_box' => true )
	);
}

add_action( 'add_meta_boxes', 'themename_blocks_add_meta_box' );

function themename_blocks_post_options_metabox_html( $post ) {
	$subtitle = get_post_meta( $post->ID, '_themename_blocks_post_subtitle', true );
	wp_nonce_field( 'themename_blocks_update_post_metabox', 'themename_blocks_update_post_nonce' );
	?>
    <p>
        <label for="themename_blocks_post_subtitle_field"><?php esc_html_e( 'Post Subtitle', 'themename_blocks' ); ?></label>
        <br/>
        <input class="widefat" type="text" name="themename_blocks_post_subtitle_field"
               id="themename_blocks_post_subtitle_field" value="<?php echo esc_attr( $subtitle ); ?>"/>
    </p>
	<?php
}

function themename_blocks_save_post_metabox( $post_id, $post ) {

	$edit_cap = get_post_type_object( $post->post_type )->cap->edit_post;
	if ( ! current_user_can( $edit_cap, $post_id ) ) {
		return;
	}
	if ( ! isset( $_POST['themename_blocks_update_post_nonce'] ) || ! wp_verify_nonce( $_POST['themename_blocks_update_post_nonce'], 'themename_blocks_update_post_metabox' ) ) {
		return;
	}

	if ( array_key_exists( 'themename_blocks_post_subtitle_field', $_POST ) ) {
		update_post_meta(
			$post_id,
			// _ before meta_key name prevents the custom-field from showing up int the Custom Fields editor in classic editor
			'_themename_blocks_post_subtitle',
			sanitize_text_field( $_POST['themename_blocks_post_subtitle_field'] )
		);
	}

}

add_action( 'save_post', 'themename_blocks_save_post_metabox', 10, 2 );