import { Component } from "@wordpress/element";
import { withSelect } from "@wordpress/data";
import { __ } from "@wordpress/i18n";
import { decodeEntities } from "@wordpress/html-entities";
import { RangeControl, PanelBody, SelectControl } from "@wordpress/components";
import { InspectorControls } from "@wordpress/editor";

class LatestPostsEdit extends Component {
    onChangeNumberOfPosts = numberOfPosts => {
        this.props.setAttributes({ numberOfPosts });
    };

    onChangeCategories = categories => {
        // We support multiple choices so we get the id:s as an array, and convert that to a comma separated list
        this.props.setAttributes({ postCategories: categories.join(",") });
    };

    render() {
        const { posts, categories, className, attributes } = this.props;
        const { numberOfPosts, postCategories } = attributes;
        return (
            <>
                <InspectorControls>
                    <PanelBody title={__("Posts Settings", "themename-blocks")}>
                        <SelectControl
                            multiple
                            label={__("Categories", "themename-blocks")}
                            // onChange={(v) => console.log(v)}
                            onChange={this.onChangeCategories}
                            options={
                                categories &&
                                categories.map(category => ({
                                    value: category.id,
                                    label: category.name
                                }))
                            }
                            value={postCategories && postCategories.split(",")} // convert comma separated list back to an array
                        />
                        <RangeControl
                            label={__("Number of posts", "themename-blocks")}
                            value={numberOfPosts}
                            onChange={this.onChangeNumberOfPosts}
                            min={1}
                            max={10}
                        />
                    </PanelBody>
                </InspectorControls>
                {posts && posts.length > 0 ? (
                    // className comes from plugin.php themename_blocks_render_latest_posts_block->$posts
                    <ul className={className}>
                        {posts.map(post => (
                            <li key={post.id}>
                                <a target="_blank" rel="noopener noreferrer" href={post.link}></a>
                                {decodeEntities(post.title.rendered)}
                            </li>
                        ))}
                    </ul>
                ) : (
                    // If there are posts, but array empty, we have no posts. If posts is null, no array, then we are loading
                    <div>
                        {posts
                            ? __("No Posts Found", "themename-blocks")
                            : __("Loading...", "themename-blocks")}
                    </div>
                )}
            </>
        );
    }
}

export default withSelect((select, props) => {
    const { attributes } = props;
    const { numberOfPosts, postCategories } = attributes;
    let query = { per_page: numberOfPosts };
    if (postCategories) {
        query["categories"] = postCategories;
    }

    return {
        posts: select("core").getEntityRecords("postType", "post", query),
        categories: select("core").getEntityRecords("taxonomy", "category", { per_page: -1 })
    };
})(LatestPostsEdit);
