import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

import edit from "./edit";

registerBlockType("themename-blocks/latest-posts", {
    title: __("Latest Posts", "themename-blocks"),
    description: __("Block showing the latest posts.", "themename-blocks"),
    icon: "admin-post",
    category: "themename-category",
    keywords: [__("posts", "themename-blocks"), __("latest", "themename-blocks")],
    edit: edit,
    save() {
        return null;
    }
});
