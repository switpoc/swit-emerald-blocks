import { registerBlockType, createBlock } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { InnerBlocks, InspectorControls } from "@wordpress/editor";
import { PanelBody, RangeControl } from "@wordpress/components";

// templateLock=all, går ej att lägga till block eller ändra ordning
// templateLock=insert, går ej att lägga till, men man kan ändra ordning

const attributes = {
    columns: {
        type: "number",
        default: 2
    }
};

registerBlockType("themename-blocks/team-members", {
    title: __("Team Members", "themename-blocks"),
    description: __("Block showing Team Members.", "themename-blocks"),
    icon: "grid-view",
    supports: {
        html: false
        //align: ['wide', 'full'] // does not play well with swit-emerald since we show the content in a box
    },
    transforms: {
        from: [
            {
                type: "block",
                blocks: ["core/gallery"],
                transform: ({ columns, images }) => {
                    //console.log(attributes);
                    let inner = images.map(({ alt, id, url }) =>
                        createBlock("themename-blocks/team-member", { alt, id, url })
                    );
                    return createBlock(
                        "themename-blocks/team-members",
                        {
                            columns: columns
                        },
                        inner
                    );
                }
            },
            {
                type: "block",
                blocks: ["core/image"],
                isMultiBlock: true,
                transform: attributes => {
                    let inner = attributes.map(({ alt, id, url }) =>
                        createBlock("themename-blocks/team-member", { alt, id, url })
                    );
                    return createBlock(
                        "themename-blocks/team-members",
                        {
                            columns: 3
                        },
                        inner
                    );
                }
            }
        ]
    },
    category: "themename-category",
    keywords: [
        __("team", "themename-blocks"),
        __("member", "themename-blocks"),
        __("person", "themename-blocks")
    ],
    attributes,
    edit({ className, attributes, setAttributes }) {
        const { columns } = attributes;
        return (
            <div className={`${className} has-${columns}-columns`}>
                <InspectorControls>
                    <PanelBody>
                        <RangeControl
                            label={__("Columns", "themename-blocks")}
                            value={columns}
                            onChange={columns => setAttributes({ columns })}
                            min={1}
                            max={6}
                        />
                    </PanelBody>
                </InspectorControls>
                <InnerBlocks
                    allowedBlocks={["themename-blocks/team-member"]}
                    template={
                        [
                            //Template creates blocks and can be populated by adding an object, like so:
                            // ["themename-blocks/team-member"]
                            // [
                            //     "themename-blocks/team-member",
                            //     { title: "Member One", info: "Info Member One" }
                            // ],
                            // [
                            //     "themename-blocks/team-member",
                            //     { title: "Member Two", info: "Info Member Two" }
                            // ]
                        ]
                    }
                    // templateLock="insert"
                />
            </div>
        );
    },

    save({ attributes }) {
        const { columns } = attributes;
        return (
            <div className={`has-${columns}-columns`}>
                <InnerBlocks.Content />
            </div>
        );
    }
});
