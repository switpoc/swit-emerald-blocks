import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { RichText } from "@wordpress/editor";
import { Dashicon } from "@wordpress/components";

import edit from "./edit";
import "./styles.editor.scss";
import "./parent";

const attributes = {
    title: {
        type: "string",
        source: "html",
        selector: "h4"
    },
    info: {
        type: "string",
        source: "html",
        selector: "p"
    },
    //    image via url
    id: {
        type: "number"
    },
    alt: {
        type: "string",
        source: "attribute",
        selector: "img",
        attribute: "alt",
        default: ""
    },
    url: {
        type: "string",
        source: "attribute",
        selector: "img",
        attribute: "src"
    },
    social: {
        type: "array",
        default: [
            // { link: "http://facebook.com", icon: "facebook" },
            // { link: "http://twitter.com", icon: "twitter" }
        ],
        source: "query",
        selector: ".wp-block-themename-blocks-team-member__social ul li",
        query: {
            icon: {
                source: "attribute",
                attribute: "data-icon"
            },
            link: {
                source: "attribute",
                selector: "a",
                attribute: "href"
            }
        }
    }
};

registerBlockType("themename-blocks/team-member", {
    title: __("Team Member", "themename-blocks"),
    description: __("Block showing a Team Member.", "themename-blocks"),
    icon: "admin-users",
    parent: ["themename-blocks/team-members"] /* => Can only add this block to team-members */,
    supports: {
        reusable: false,
        html: false
    },
    category: "themename-category",
    keywords: [
        __("team", "themename-blocks"),
        __("member", "themename-blocks"),
        __("person", "themename-blocks")
    ],
    attributes,
    save: ({ attributes }) => {
        const { title, info, url, alt, id, social } = attributes;
        return (
            <div>
                {url && <img src={url} alt={alt} className={id ? `wp-image-${id}` : null} />}
                {title && (
                    <RichText.Content
                        className={"wp-block-themename-blocks-team-member__title"}
                        tagName="h4"
                        value={title}
                    />
                )}
                {info && (
                    <RichText.Content
                        className={"wp-block-themename-blocks-team-member__info"}
                        tagName="p"
                        value={info}
                    />
                )}
                {social.length > 0 && (
                    <div className={"wp-block-themename-blocks-team-member__social"}>
                        <ul>
                            {social.map((item, index) => {
                                return (
                                    <li key={index} data-icon={item.icon}>
                                        <a
                                            href={item.link}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <Dashicon icon={item.icon} size={16} />
                                        </a>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                )}
            </div>
        );
    },
    edit
});
