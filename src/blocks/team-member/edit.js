import { Component } from "@wordpress/element";
import {
    RichText,
    MediaPlaceholder,
    BlockControls,
    MediaUpload,
    MediaUploadCheck,
    InspectorControls,
    URLInput
} from "@wordpress/editor";
import { __ } from "@wordpress/i18n";
import { isBlobURL } from "@wordpress/blob";
import {
    Spinner,
    withNotices,
    Toolbar,
    IconButton,
    PanelBody,
    TextareaControl,
    SelectControl,
    Dashicon,
    Tooltip,
    TextControl
} from "@wordpress/components";
import { withSelect } from "@wordpress/data";
import { SortableContainer, SortableElement, arrayMove } from "react-sortable-hoc";

class TeamMemberEdit extends Component {
    state = {
        selectedLink: null
    };

    componentDidMount() {
        const { attributes, setAttributes } = this.props;
        const { url, id } = attributes;
        if (url && isBlobURL(url) && !id) {
            // this is an unfinished upload. Let's clear it...
            setAttributes({
                url: "",
                alt: ""
            });
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isSelected && !this.props.isSelected) {
            //writer has unselected a member, clear selected social icon, if any.
            this.setState({
                selectedLink: null
            });
        }
    }

    onChangeTitle = title => {
        this.props.setAttributes({ title });
    };

    onChangeInfo = info => {
        this.props.setAttributes({ info });
    };

    onSelectImage = ({ id, url, alt }) => {
        this.props.setAttributes({
            id,
            url,
            alt
        });
    };

    onSelectUrl = url => {
        this.props.setAttributes({
            url,
            id: null,
            alt: ""
        });
    };

    onUploadError = message => {
        // console.log(message);
        const { noticeOperations } = this.props;
        noticeOperations.createErrorNotice(message);
    };

    removeImage = () => {
        this.props.setAttributes({
            id: null,
            url: "",
            alt: ""
        });
    };
    //This updates the alt text for the POST, not the image it self.
    // So it will not change the alt text in the image library
    updateAlt = alt => {
        this.props.setAttributes({
            alt
        });
    };
    onImageSizeChange = url => {
        this.props.setAttributes({
            url
        });
    };

    getImageSizes() {
        const { image, imageSizes } = this.props;
        if (!image) return [];
        let options = [];
        const sizes = image.media_details.sizes;
        for (const key in sizes) {
            const size = sizes[key];
            const imageSize = imageSizes.find(size => size.slug === key);
            if (imageSize) {
                options.push({
                    label: imageSize.name,
                    value: size.source_url
                });
            }
        }
        return options;
    }

    addNewLink = () => {
        const { setAttributes, attributes } = this.props;
        const { social } = attributes;
        setAttributes({
            social: [...social, { icon: "", link: "" }]
        });
        this.setState({
            //set this link (= last item in array) to selected
            selectedLink: social.length
        });
    };

    updateSocialItem = (type, value) => {
        const { setAttributes, attributes } = this.props;
        const { social } = attributes;
        const { selectedLink } = this.state;
        let new_social = [...social];
        new_social[selectedLink][type] = value;
        setAttributes({ social: new_social });
    };

    removeLink = e => {
        e.preventDefault();
        const { setAttributes, attributes } = this.props;
        const { social } = attributes;
        const { selectedLink } = this.state;
        setAttributes({
            social: [...social.slice(0, selectedLink), ...social.slice(selectedLink + 1)]
        });
        this.setState({
            selectedLink: null
        });
    };

    onSortEnd = (oldIndex, newIndex) => {
        const { setAttributes, attributes } = this.props;
        const { social } = attributes;
        let new_social = arrayMove(social, oldIndex, newIndex);
        setAttributes({ social: new_social });
        this.setState({ selectedLink: null }); // remove red frame of selected item when dropping the icon in place
    };

    render() {
        //console.log(this.props);
        const { className, attributes, noticeUI, isSelected } = this.props;
        const { title, info, url, alt, id, social } = attributes;

        const SortableList = SortableContainer(() => {
            return (
                <ul>
                    {social.map((item, index) => {
                        let SortableItem = SortableElement(() => {
                            return (
                                <li
                                    key={index}
                                    onClick={() => this.setState({ selectedLink: index })}
                                    className={
                                        this.state.selectedLink === index ? "is-selected" : null
                                    }
                                >
                                    <Dashicon icon={item.icon ? item.icon : "no-alt"} size={16} />
                                </li>
                            );
                        });
                        return <SortableItem key={index} index={index} />;
                    })}
                    {isSelected && (
                        <li className={"wp-block-themename-blocks-team-member__addIconLI"}>
                            <Tooltip text={__("Add Item", "themename-blocks")}>
                                <button
                                    className={"wp-block-themename-blocks-team-member__addIcon"}
                                    onClick={this.addNewLink}
                                >
                                    <Dashicon icon={"plus"} size={14} />
                                </button>
                            </Tooltip>
                        </li>
                    )}
                </ul>
            );
        });

        return (
            <>
                <InspectorControls>
                    <PanelBody title={__("Image Settings", "themename-blocks")}>
                        {url &&
                        !isBlobURL(url) && ( // only show if we have an uploaded image
                                <TextareaControl
                                    label={__("Alt Text (Alternative Text)", "themename-blocks")}
                                    value={alt}
                                    onChange={this.updateAlt}
                                    help={__(
                                        "Alternative text describes your image to people who can´t see it. Add a short description with its key details."
                                    )}
                                />
                            )}
                        {id && (
                            <SelectControl
                                label={__("Image Size", "themename-blocks")}
                                options={this.getImageSizes()}
                                onChange={this.onImageSizeChange}
                                value={url}
                            />
                        )}
                    </PanelBody>
                </InspectorControls>
                <BlockControls>
                    {url && !isBlobURL(url) && (
                        <Toolbar>
                            {id && ( // only show edit btn if the img is in media library (not when external url)
                                <MediaUploadCheck>
                                    <MediaUpload
                                        onSelect={this.onSelectImage}
                                        allowedType={["image"]}
                                        value={id}
                                        render={({ open }) => {
                                            return (
                                                <IconButton
                                                    className={
                                                        "components-icon-button components-toolbar__control"
                                                    } // To make hover effect same as other buttons
                                                    label={__("Edit Image", "themename-blocks")}
                                                    onClick={open}
                                                    icon="edit"
                                                />
                                            );
                                        }}
                                    />
                                </MediaUploadCheck>
                            )}
                            <IconButton
                                className={"components-icon-button components-toolbar__control"} // To make hover effect same as other buttons
                                label={__("Remove Image", "themename-blocks")}
                                onClick={this.removeImage}
                                icon="trash"
                            />
                        </Toolbar>
                    )}
                </BlockControls>
                <div className={className}>
                    {url ? (
                        <>
                            <img src={url} alt={alt} />
                            {isBlobURL(url) && <Spinner />}
                        </>
                    ) : (
                        <MediaPlaceholder
                            icon="format-image"
                            // onSelect={(image) => console.log(image)}
                            onSelect={this.onSelectImage}
                            // onSelectURL={url => console.log(url)}
                            onSelectURL={this.onSelectUrl}
                            // onError={message => console.log(message)}
                            onError={this.onUploadError}
                            accept="image/*"
                            allowedTypes={["image"]}
                            notices={noticeUI}
                        />
                    )}
                    <RichText
                        className={"wp-block-themename-blocks-team-member__title"}
                        tagName="h4"
                        onChange={this.onChangeTitle}
                        value={title}
                        placeholder={__("Member Name", "themename-blocks")}
                        formattingControls={[]}
                    />
                    <RichText
                        className={"wp-block-themename-blocks-team-member__info"}
                        tagName="p"
                        onChange={this.onChangeInfo}
                        value={info}
                        placeholder={__("Member Info", "themename-blocks")}
                        formattingControls={[]}
                    />
                    <div className={"wp-block-themename-blocks-team-member__social"}>
                        <SortableList
                            axis="x"
                            helperClass={"social_dragging"}
                            distance={10}
                            onSortEnd={({ oldIndex, newIndex }) =>
                                this.onSortEnd(oldIndex, newIndex)
                            }
                        />
                        {/* <ul>
                            {social.map((item, index) => {
                                return (
                                    <li
                                        key={index}
                                        onClick={() => this.setState({ selectedLink: index })}
                                        className={
                                            this.state.selectedLink === index ? "is-selected" : null
                                        }
                                    >
                                        <Dashicon icon={item.icon} size={16} />
                                    </li>
                                );
                            })}
                            {isSelected && (
                                <li className={"wp-block-themename-blocks-team-member__addIconLI"}>
                                    <Tooltip text={__("Add Item", "themename-blocks")}>
                                        <button
                                            className={
                                                "wp-block-themename-blocks-team-member__addIcon"
                                            }
                                            onClick={this.addNewLink}
                                        >
                                            <Dashicon icon={"plus"} size={14} />
                                        </button>
                                    </Tooltip>
                                </li>
                            )}
                        </ul>*/}
                    </div>
                    {this.state.selectedLink !== null && (
                        <div className={"wp-block-themename-blocks-team-member__linkForm"}>
                            <TextControl
                                label={__("Icon", "themename-blocks")}
                                value={social[this.state.selectedLink].icon}
                                onChange={icon => this.updateSocialItem("icon", icon)}
                            />
                            <TextControl
                                label={__("URL", "themename-blocks")}
                                value={social[this.state.selectedLink].link}
                                onChange={url => this.updateSocialItem("link", url)}
                            />
                            <a
                                className={"wp-block-themename-blocks-team-member__removeLink"}
                                onClick={this.removeLink}
                            >
                                {__("Remove Link", "themename-blocks")}
                            </a>
                        </div>
                    )}
                </div>
            </>
        );
    }
}

export default withSelect((select, props) => {
    const id = props.attributes.id;
    return {
        image: id ? select("core").getMedia(id) : null,
        imageSizes: select("core/editor").getEditorSettings().imageSizes
    };
})(withNotices(TeamMemberEdit));
