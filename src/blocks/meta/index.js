import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { TextControl } from "@wordpress/components";

registerBlockType("themename-blocks/meta", {
    title: __("Meta Block", "themename-blocks"),
    description: __("Meta Block for editing meta field", "themename-blocks"),
    icon: "admin-tools",
    category: "themename-category",
    attributes: {
        post_subtitle: {
            type: "string",
            source: "meta",
            meta: "_themename_blocks_post_subtitle"
        }
    },
    edit({ attributes, setAttributes }) {
        function onChange(value) {
            setAttributes({ post_subtitle: value });
        }
        return (
            <div>
                <TextControl
                    label={__("Post Subtitle", "themename-blocks")}
                    value={attributes.post_subtitle}
                    onChange={onChange}
                />
            </div>
        );
    },
    save() {
        return null;
    }
});
