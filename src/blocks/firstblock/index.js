const registerBlockType = wp.blocks.registerBlockType;
const __ = wp.i18n.__;
const el = wp.element.createElement;

registerBlockType("themename-blocks/firstblock", {
    title: __("First Block", "themename-blocks"),
    description: __("Our First Block", "themename-blocks"),
    category: "layout",
    icon: {
        background: "#535454",
        foreground: "#fff",
        src: "admin-network"
    },
    keywords: [__("photo", "themename-blocks"), __("image", "themename-blocks")],
    edit: function () {
        return el("p", null, "Editor");
    },
    save: function () {
        return el("p", null, "Saved Content");
    }
});
