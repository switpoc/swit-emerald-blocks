import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

import edit from "./edit";
// Second block for todo list. We import it here so it get's imported in the plugin. Could be put in a separate folder
// Still needs to be registered in plugin.php though
import "./ToDoInfo";

registerBlockType("themename-blocks/todo-list", {
    title: __("Redux Todo List", "themename-blocks"),
    description: __("A todo list", "themename-blocks"),
    icon: "editor-ul",
    category: "themename-category",
    edit,
    save() {
        return null;
    }
});
