import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { withSelect } from "@wordpress/data";

let ToDoCount = props => {
    return (
        <div>
            <p>Total: {props.total}</p>
            <p>To Do: {props.todo}</p>
            <p>Done: {props.done}</p>
        </div>
    );
};

// Wrap a functional component with withSelect
ToDoCount = withSelect(select => {
    return {
        total: select("themename-blocks/todo").getToDosNumber(),
        todo: select("themename-blocks/todo").getUnDoneToDosNumber(),
        done: select("themename-blocks/todo").getDoneToDosNumber()
    };
})(ToDoCount);

registerBlockType("themename-blocks/todo-list-count", {
    title: __("Redux ToDo Count", "themename-blocks"),
    description: __("Redux ToDo Count", "themename-blocks"),
    icon: "editor-ul",
    category: "themename-category",
    edit() {
        return <ToDoCount />;
    },
    save() {}
});
