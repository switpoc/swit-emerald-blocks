import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";

registerBlockType("themename-blocks/todo-list-count", {
    title: __("Redux ToDo Count", "themename-blocks"),
    description: __("Redux ToDo Count", "themename-blocks"),
    icon: "editor-ul",
    category: "themename-category",
    edit() {},
    save() {}
});
