import "./styles.editor.scss";
import { registerBlockType, createBlock } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { RichText, getColorClassName } from "@wordpress/editor";
import Edit from "./edit";
import classnames from "classnames";
import { omit } from "lodash";

const attributes = {
    content: {
        type: "string",
        source: "html",
        selector: "h4"
    },
    textAlignment: {
        type: "string"
    },
    backgroundColor: {
        type: "string"
    },
    textColor: {
        type: "string"
    },
    customBackgroundColor: {
        type: "string"
    },
    customTextColor: {
        type: "string"
    },
    shadow: {
        type: "boolean",
        default: false
    },
    shadowOpacity: {
        type: "number",
        default: 0.3
    }
};

registerBlockType("themename-blocks/secondblock", {
    title: __("Second Block", "themename-blocks"),
    description: __("Our Second Block, with ES6", "themename-blocks"),
    //category: "layout",
    category: "themename-category",
    icon: {
        background: "#535454",
        foreground: "#fff",
        src: (
            <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-7 9h-2V5h2v6zm0 4h-2v-2h2v2z" />
                <path d="M0 0h24v24H0z" fill="none" />
            </svg>
        )
    },
    keywords: [__("photo", "themename-blocks"), __("image", "themename-blocks")],
    styles: [
        {
            name: "rounded",
            label: __("Rounded", "themename-blocks"),
            isDefault: true
        },
        {
            name: "outline",
            label: __("Outline", "themename-blocks")
        },
        {
            name: "squared",
            label: __("Squared", "themename-blocks")
        }
    ],
    attributes,
    // simulating changing p-tag to h4 in attr, edit and save
    // in deprecated you have to put all attributes, even if they have not changed. + supports if used + old version of save.
    deprecated: [
        {
            // replacing the old alignment, with new textAlignment
            attributes: omit(
                {
                    ...attributes,
                    alignment: {
                        type: "string"
                    }
                },
                ["textAlignment"]
            ),
            migrate: attributes => {
                return omit(
                    {
                        ...attributes,
                        textAlignment: attributes.alignment // tells guthenberg that textAlignment used to be called alignment
                    },
                    ["alignment"]
                ); // remove alignment key since it's no longer used
            },
            save: ({ attributes }) => {
                const {
                    content,
                    alignment,
                    backgroundColor,
                    textColor,
                    customBackgroundColor,
                    customTextColor,
                    shadow,
                    shadowOpacity
                } = attributes;

                const backgroundClass = getColorClassName("background-color", backgroundColor);
                const textClass = getColorClassName("color", textColor);

                const classes = classnames({
                    [backgroundClass]: backgroundClass,
                    [textClass]: textClass,
                    "has-shadow": shadow,
                    [`shadow-opacity-${shadowOpacity * 100}`]: shadowOpacity
                });

                return (
                    <RichText.Content
                        tagName="h4"
                        className={classes}
                        value={content}
                        style={{
                            textAlign: alignment,
                            backgroundColor: backgroundClass ? undefined : customBackgroundColor,
                            color: textClass ? undefined : customTextColor
                        }}
                    />
                );
            }
        },
        {
            // this migration should also have the above migration, in case someone skips an upgrade
            //supports,
            attributes: omit(
                {
                    ...attributes,
                    content: {
                        type: "string",
                        source: "html",
                        selector: "p"
                    }
                },
                ["textAlignment"]
            ),
            migrate: attributes => {
                return omit(
                    {
                        ...attributes,
                        textAlignment: attributes.alignment // tells guthenberg that textAlignment used to be called alignment
                    },
                    ["alignment"]
                ); // remove alignment key since it's no longer used
            },
            save: ({ attributes }) => {
                const {
                    content,
                    alignment,
                    backgroundColor,
                    textColor,
                    customBackgroundColor,
                    customTextColor,
                    shadow,
                    shadowOpacity
                } = attributes;

                const backgroundClass = getColorClassName("background-color", backgroundColor);
                const textClass = getColorClassName("color", textColor);

                const classes = classnames({
                    [backgroundClass]: backgroundClass,
                    [textClass]: textClass,
                    "has-shadow": shadow,
                    [`shadow-opacity-${shadowOpacity * 100}`]: shadowOpacity
                });

                return (
                    <RichText.Content
                        tagName="p"
                        className={classes}
                        value={content}
                        style={{
                            textAlign: alignment,
                            backgroundColor: backgroundClass ? undefined : customBackgroundColor,
                            color: textClass ? undefined : customTextColor
                        }}
                    />
                );
            }
        }
    ],
    transforms: {
        from: [
            {
                type: "block",
                blocks: ["core/paragraph"],
                transform: ({ content, align }) => {
                    console.log(content);
                    return createBlock("themename-blocks/secondblock", {
                        content: content,
                        textAlignment: align
                    });
                }
            },
            {
                // this adds a shortcut to create the block. I normal block add # + space and the block will be created.
                type: "prefix",
                prefix: "#",
                transform: attributes => {
                    return createBlock("themename-blocks/secondblock", {});
                }
            }
        ],
        to: [
            {
                type: "block",
                blocks: ["core/paragraph"],
                isMatch: ({ content }) => {
                    return !!content;
                },
                transform: ({ content, textAlignment }) => {
                    console.log(content);
                    return createBlock("core/paragraph", {
                        content: content,
                        align: textAlignment
                    });
                }
            }
        ]
    },
    edit: Edit,
    save: ({ attributes }) => {
        const {
            content,
            textAlignment,
            backgroundColor,
            textColor,
            customBackgroundColor,
            customTextColor,
            shadow,
            shadowOpacity
        } = attributes;

        const backgroundClass = getColorClassName("background-color", backgroundColor);
        const textClass = getColorClassName("color", textColor);

        const classes = classnames({
            [backgroundClass]: backgroundClass,
            [textClass]: textClass,
            "has-shadow": shadow,
            [`shadow-opacity-${shadowOpacity * 100}`]: shadowOpacity
        });

        return (
            <RichText.Content
                tagName="h4"
                className={classes}
                value={content}
                style={{
                    textAlign: textAlignment,
                    backgroundColor: backgroundClass ? undefined : customBackgroundColor,
                    color: textClass ? undefined : customTextColor
                }}
            />
        );
    }
});
