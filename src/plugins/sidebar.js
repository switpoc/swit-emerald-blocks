import { registerPlugin } from "@wordpress/plugins";
import { PluginSidebar, PluginSidebarMoreMenuItem } from "@wordpress/edit-post";
import { __ } from "@wordpress/i18n";

//adds an extra sidebar to customize
registerPlugin("themename-blocks-sidebar", {
    icon: "smiley",
    render: () => {
        return (
            <>
                {/*To add the sidebar to the menu, so it can be restored if someone hides it.*/}
                <PluginSidebarMoreMenuItem target="themename-blocks-sidebar">
                    {__("Meta Options", "themename-blocks")}
                </PluginSidebarMoreMenuItem>

                <PluginSidebar
                    //name could be anything but needs to be same as above target for that menu to work
                    name="themename-blocks-sidebar"
                    //down icon overrides above...
                    icon="admin-post"
                    title={__("Meta Options", "themename-blocks")}
                >
                    kfksafkölk
                </PluginSidebar>
            </>
        );
    }
});
